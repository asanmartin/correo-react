
CREATE database correo_db;
USE correo_db;
--
CREATE TABLE postal(
    code INT NOT NULL,
    location VARCHAR(50),
    PRIMARY KEY (code)
);

INSERT INTO postal(code, location)
    VALUES
        (8000, 'Bahía Blanca'),
        (1900, 'La Plata'),
        (8153, 'Monte Hermoso');
--
CREATE TABLE company(
    id INT NOT NULL AUTO_INCREMENT,
    company_name VARCHAR(20),
    PRIMARY KEY(id)
);

INSERT INTO company( id, company_name)
    VALUES
        (1, 'Correo Argentino'),
        (2, 'Oca'),
        (3, 'Andreani');
--
CREATE TABLE sender(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    address_name VARCHAR(50),
    address_num INT,
    postal_code INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (postal_code) REFERENCES postal(code)
);

INSERT INTO sender (name, address_name, address_num, postal_code)
    VALUES
        ('municipalidad de bahia blanca', 'Alsina', 65, 8000),
        ('infraestructura', 'Alsina', 65, 8000),
        ('cultura', 'Alsina', 62, 8000),
        ('secreta,ria privada', 'Alsina', 65, 8000);
--
CREATE TABLE receiver(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    address_name VARCHAR(50),
    address_num INT,
    postal_code INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (postal_code) REFERENCES postal(code)
);
INSERT INTO receiver (name, address_name, address_num, postal_code)
    VALUES
        ('municipalidad de monte hermoso', 'nelida maria fossatty centro', 0, 8153),
        ('juzgado de familia n2', 'Calle 46', 870, 1900);
--
CREATE TABLE items(
    id INT NOT NULL AUTO_INCREMENT,
    sender_id INT NOT NULL,
    receiver_id INT NOT NULL,
    company INT NOT NULL,
    code VARCHAR(15),
    PRIMARY KEY(id),
    FOREIGN KEY (sender_id) REFERENCES sender(id),
    FOREIGN KEY (receiver_id) REFERENCES receiver(id),
    FOREIGN KEY (company) REFERENCES company(id)
);
INSERT INTO items( sender_id, receiver_id, company, code)
    VALUES
        (1, 1, 1, 'CU304743948'),
        (1, 1, 1, 'CP112766450'),
        (2, 2, 1, 'EP737019724'),
        (3, 2, 2, '658394319729'),
        (1, 2, 2, '658394319729'),
        (4, 1, 3, '6600037513647');
--
-- simple select
SELECT 
    id,
    name,
    CONCAT(address_name, ': ',address_num) AS 'dirección',
    postal_code
FROM sender;
--
SELECT 
    i.id, 
    s.name AS remitente,
    CONCAT( s.address_name, ', ', s.address_num ) AS 'S: dirección',
    r.name AS destinatario,
    CONCAT( r.address_name, ', ', r.address_num ) AS 'R: dirección',
    i.company AS 'Despachado por',
    i.code AS 'código'
FROM items AS i
INNER JOIN sender AS s
ON s.id = i.sender_id
INNER JOIN receiver AS r
ON r.id = i.receiver_id
;

ORDER BY d.id
LIMIT 7
