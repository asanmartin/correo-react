const express = require('express')
const routes = express.Router()

routes.get('/', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        conn.query('SELECT * FROM data', (err, rows)=>{
            if (err) return res.send(err)

            res.json(rows)
        })   
    })
})

routes.post('/', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        //console.log(req.body)
        conn.query('INSERT INTO registro set ?', [req.body], (err, rows)=>{
            if (err) return res.send(err)

            res.send('registrado!')
        })   
    })
})

routes.get('/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        conn.query('SELECT * FROM registro WHERE id = ?', [req.params.id], (err,row) =>{
            if (err) return res.send(err)

            res.json(row)
        })
    })
})

routes.delete('/:id', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        //console.log(req.body)
        conn.query('DELETE FROM registro WHERE id = ?', [req.params.id], (err, rows)=>{
            if (err) return res.send(err)

            res.send('eliminado!')
        })   
    })
})

routes.put('/:id', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        //console.log(req.body)
        conn.query('UPDATE registro set ? WHERE id = ?', [res.body, req.params.id], (err, rows)=>{
            if (err) return res.send(err)

            res.send('actualizado!')
        })   
    })
})

module.exports = routes